import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
// import Youtube from './Tugas/Tugas12/App';
// import Login from './Tugas/Tugas13/login';
// import Profile from './Tugas/Tugas13/profil';
// import Tugas14 from './Tugas/Tugas14/App';
// import Tugas15 from './Tugas/Tugas15/index';
import TugasNavigation from './Tugas/Tugas15/TugasNavigation/index';
import Quiz3 from './Quiz3/index';


export default function App() {
  return (
    <View style={styles.container}>
      <Quiz3 />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
