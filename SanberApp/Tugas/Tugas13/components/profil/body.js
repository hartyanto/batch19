import { StatusBar } from 'expo-status-bar';
import React from 'react';
import Icon from "react-native-vector-icons/MaterialIcons"
import { Image, ImageBackground, StyleSheet, Text, TouchableOpacity, View } from 'react-native';

export default function profil() {
  return (
    <View style={styles.container}>
        <View style={styles.accountDetail}>
            <Text style={styles.fullname}>Nama Akun My Portfolio</Text>
            <Text style={styles.pekerjaan}>Pekerjaan</Text>
            <View style={styles.socialMedia}>
                <Text style={{marginBottom: 10, fontWeight: '600',}}>Akun Media Sosial</Text>
                <View style={styles.listMediaSocial}>
                    <Image source={require('../../images/fb.png')} />
                    <Text style={{marginLeft: 10}}>Nama Facebook Akun</Text>
                </View>
                <View style={styles.listMediaSocial}>
                    <Image source={require('../../images/instagram.png')} />
                    <Text style={{marginLeft: 10}}>Nama Instagram Akun</Text>
                </View>
                <View style={styles.listMediaSocial}>
                    <Image source={require('../../images/twitter.png')} />
                    <Text style={{marginLeft: 10}}>Nama Twitter Akun</Text>
                </View>
            </View>
        </View>
        <View style={styles.portfolio}>
            <Text style={styles.titlePort}>Portfoilo Saya</Text>
            <View style={styles.portCont} >
                <Image source={require('../../images/port.png')} />
                <View style={styles.detailPort}>
                    <Text style={{fontSize:18, marginBottom: 5,}}>Nama Project</Text>
                    <Text style={{fontSize:13}}>Penjelasan singkat Project
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Quis, cupiditate?
                    </Text>
                </View>
            </View>
            <View style={styles.portCont} >
                <Image source={require('../../images/port.png')} />
                <View style={styles.detailPort}>
                    <Text style={{fontSize:18, marginBottom: 5,}}>Nama Project</Text>
                    <Text style={{fontSize:13}}>Penjelasan singkat Project
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Quis, cupiditate?
                    </Text>
                </View>
            </View>
            <View style={styles.portCont} >
                <Image source={require('../../images/port.png')} />
                <View style={styles.detailPort}>
                    <Text style={{fontSize:18, marginBottom: 5,}}>Nama Project</Text>
                    <Text style={{fontSize:13}}>Penjelasan singkat Project
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Quis, cupiditate?
                    </Text>
                </View>
            </View>
            <View style={styles.portCont} >
                <Image source={require('../../images/port.png')} />
                <View style={styles.detailPort}>
                    <Text style={{fontSize:18, marginBottom: 5,}}>Nama Project</Text>
                    <Text style={{fontSize:13}}>Penjelasan singkat Project
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Quis, cupiditate?
                    </Text>
                </View>
            </View>
        </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  fullname: {
      letterSpacing: 2,
      fontSize: 18,
      textAlign: "center",
      fontWeight: "bold",
  },
  pekerjaan: {
      fontSize: 13,
      fontWeight: '600',
      textAlign: 'center',
      marginTop: 5,
  },
  socialMedia: {
      marginTop: 20,
      paddingHorizontal: 20,
  },
  listMediaSocial: {
      flexDirection: 'row',
      marginBottom: 10,
  },
  portfolio: {
    marginTop: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  titlePort: {
      fontSize: 18,
      textAlign: 'center',
      marginBottom: 10,
  },
  portCont: {
      marginBottom: 10,
      flexDirection: 'row',
      alignItems: 'center',
      paddingHorizontal: 10,
      width: 350,
      height: 125,
      backgroundColor: '#F6F6F6'
  },
  detailPort: {
      width: 200,
      paddingHorizontal: 10,
  }
});