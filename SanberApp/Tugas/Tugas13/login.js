import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native';

export default function login() {
  return (
    <View style={styles.container}>
      <View style={styles.logoCont}>
            <Image source={require('./images/Logo.svg')} style={styles.logo}/>
      </View>
      <View style={styles.loginCont}>
        <View style={styles.form}>
          <Text style={styles.formLabel}>Username / Email</Text>
        </View>
        <View style={styles.form}>
          <Text style={styles.formLabel}>Password</Text>
          <TouchableOpacity>
            <Text style={{color: '#75C4F1'}}>Show</Text>
          </TouchableOpacity>
        </View>
        <TouchableOpacity>
          <View style={styles.button}>
            <Text style={styles.labelButton}>Log In</Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity>
          <Text style={styles.forgotPass}>Forgot your password?</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  logoCont: {
      flexDirection: "row",
      justifyContent: 'center',
      height:400,
      backgroundColor: '#75C4F1',
      borderBottomLeftRadius: 30,
      borderBottomRightRadius: 30,
  },
  logo: {
    alignSelf: 'center',
  },
  loginCont: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignContent: 'center',
  },
  form: {
    marginHorizontal: 40,
    marginBottom: 25,
    paddingHorizontal: 15,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: '#F6F6F6',
    borderRadius: 6,
    width: 305,
    height: 45,
  },
  formLabel: {
    color: '#BDBDBD',
  },
  button: {
    marginHorizontal: 25,
    marginTop: 70,
    flexDirection: 'row',
    justifyContent: 'center',
    width: 345,
    height: 50,
    backgroundColor: '#75C4F1',
    borderRadius: 100,
  },
  labelButton: {
    alignSelf: 'center',
    color: '#FFFFFF',
    fontSize: 16,
    fontWeight: "600",
  },
  forgotPass: {
    fontSize: 16,
    color: '#75C4F1',
    textAlign: 'center',
    marginTop: 20,
  }
});