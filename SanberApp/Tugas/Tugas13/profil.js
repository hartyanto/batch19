import { StatusBar } from 'expo-status-bar';
import React from 'react';
import Icon from "react-native-vector-icons/MaterialIcons"
import { Image, ImageBackground, ScrollView, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import Body from './components/profil/body'

export default function profil() {
  return (
    <View style={styles.container}>
        <ScrollView>
            <View style={{height: 350}}>
                <ImageBackground style={styles.header} source={require('./images/profilBackground.png')}>
                    <View style={styles.titleCont}>
                        <Text style={styles.title}>Profile</Text>
                        <TouchableOpacity>
                            <Icon name="menu" size={25} style={{color: '#FFFFFF'}}/>
                        </TouchableOpacity>
                    </View>
                    <Image style={styles.imageCont} source={require('./images/photo.png')} />
                </ImageBackground>
            </View>
            <View style={styles.body} >
                <Body />
            </View>
        </ScrollView>

        <View style={styles.navbar}>
            <TouchableOpacity>
                <View style={styles.navItem}>
                    <Icon name="home" size={25} style={{color: '#CACACA'}} />
                    <Text style={styles.labelItem}>Home</Text>
                </View>
            </TouchableOpacity>
            <TouchableOpacity>
                <View style={styles.navItem}>
                    <Icon name="person" size={25} style={{color: '#FFFFFF'}} />
                    <Text style={styles.labelItemActive}>Profil</Text>
                </View>
            </TouchableOpacity>
            <TouchableOpacity>
                <View style={styles.navItem}>
                    <Icon name="list" size={25} style={{color: '#CACACA'}} />
                    <Text style={styles.labelItem}>Skill</Text>
                </View>
            </TouchableOpacity>
        </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    backgroundColor: 'red',
    height: 300,
  },
  titleCont: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginHorizontal: 30,
    marginTop: 20,
  },
  title: {
      fontSize: 30,
      color: '#FFFFFF',
      textAlign: 'center',
  },
  imageCont: {
      bottom: '-40%',
      left: '25%',
  },
  body: {
    flex: 1,
  },
  navbar: {
      flexDirection: 'row',
      justifyContent: 'space-around',
      alignItems: 'center',
      height: 67,
      borderTopLeftRadius: 20,
      borderTopRightRadius: 20,
      backgroundColor: '#75C4F1',
  },
  navItem: {
      alignItems: 'center',
      justifyContent: 'center'
  },
  labelItem: {
      color: '#CACACA'
  },
  labelItemActive: {
      color: '#FFFFFF'
  }
});