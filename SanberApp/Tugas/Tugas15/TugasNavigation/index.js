import React from 'react'
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import { createDrawerNavigator } from '@react-navigation/drawer';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { About, Add, Login, Project, Skill } from './pages/index'

const LoginStack = createStackNavigator();
const Drawer = createDrawerNavigator()
const Tabs = createBottomTabNavigator()

const TabsScreen = () => (
    <Tabs.Navigator>
            <Tabs.Screen name="Skill" component={Skill} />
            <Tabs.Screen name="Project" component={Project} />
            <Tabs.Screen name="Add" component={Add} />
    </Tabs.Navigator>
)

const DrawerScreen = () => (
    <Drawer.Navigator>
        <Drawer.Screen name="Home" component={TabsScreen} />
        <Drawer.Screen name="About" component={About} />
    </Drawer.Navigator>
)


export default () => (
    <NavigationContainer>
        <LoginStack.Navigator>
            <LoginStack.Screen name='Login' component={Login} />
            <LoginStack.Screen name='SkillScreen' component={DrawerScreen} options={{ title: 'Skill Page'}} />
        </LoginStack.Navigator>
    </NavigationContainer>
)