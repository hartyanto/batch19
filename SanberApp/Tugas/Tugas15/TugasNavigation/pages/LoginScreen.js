import React from 'react'
import { Button, StyleSheet, Text, View } from "react-native";


export default function Login({ navigation }) {
    return (
        <View style={styles.container}>
            <Text>Login Page</Text>
            <Button title="To skill screen" onPress={() => navigation.push('SkillScreen')} style={styles.btn} />
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    btn: {
        paddingHorizontal: 2,
        backgroundColor: 'blue',
        marginVertical: 5,
    }
})