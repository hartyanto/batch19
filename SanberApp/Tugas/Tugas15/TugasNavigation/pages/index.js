import React from 'react'
import Add from './AddScreen'
import About from './AboutScreen'
import Login from './LoginScreen'
import Project from  './ProjectScreen'
import Skill from './SkillScreen'

export {Add, About, Login, Project, Skill}