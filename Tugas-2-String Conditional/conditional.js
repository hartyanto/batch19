// =========== Soal else if ===========
// Nama dan peran diisi manual dan bisa diisi apa saja
// Nama tidak perlu dicek persis sesuai dengan input/output
// Buat kondisi if-else untuk masing-masing peran

var names = ["John", "Jane", "Jenita", "Junaedi"]
var roles = ["Penyihir", "Guard", "Werewolf", "Doctor"]

var min = 1
var max = 4
var namaRandom = Math.floor(Math.random() * (max - min) ) + min;
var peranRandom = Math.floor(Math.random() * (max - min) ) + min;
var nama = names[namaRandom]
var peran = roles[peranRandom]


console.log("----- Output soal else if -----")
// Nama tidak diisi
if( nama == "" && peran == "") {
    console.log('Nama harus diisi')
} else if( nama.toLowerCase() == 'john') {
    // Nama diisi dengan John
    console.log(`Halo ${nama}, Pilih peranmu untuk memulai game!`)
} else if( nama.toLowerCase() == 'jane' && peran.toLowerCase() == 'penyihir' ) {
    // Nama diisi dengan Jane dan peran diisi penyihir
    console.log(`Selamat datang di Dunia Werewolf, ${nama}`)
    console.log(`Halo Penyihir ${nama}, kamu dapat melihat siapa yang menjadi werewolf!`)
} else if ( nama.toLowerCase() == 'jenita' && peran.toLowerCase() == 'guard' ) {
    // Nama diisi dengan Jenita dan peran diisi guard
    console.log(`Selamat datang di Dunia Werewolf, ${nama}`)
    console.log(`Halo Guard ${nama}, kamu akan membantu melindungi temanmu dari serangan werewolf.`)
} else if ( nama.toLowerCase() == 'junaedi' && peran.toLowerCase() == 'werewolf' ) {
    // Nama diisi dengan Junaedi dan peran diisi werewolf
    console.log(`Selamat datang di Dunia Werewolf, ${nama}`)
    console.log(`Halo Werewolf ${nama}, Kamu akan memakan mangsa setiap malam!`)
} else {
    // Nama diisi
    console.log(`Halo ${nama}, Selamat datang di Dunia Werewolf!`)
    console.log(`${nama}, kamu mendapatkan peran sebagai ${peran}`)
}



// =========== Soal switch case ===========
// var hari = 21; 
// var bulan = 1; 
// var tahun = 1945;
// Maka hasil yang akan tampil di console adalah: '21 Januari 1945';

var minTanggal = 1
var maxTanggal = 31
var tanggalRandom = Math.floor(Math.random() * (maxTanggal - minTanggal + 1) ) + minTanggal;
var tanggal = tanggalRandom; // assign nilai variabel tanggal disini! (dengan angka antara 1 - 31)
var minBulan = 1
var maxBulan = 12
var bulanRandom = Math.floor(Math.random() * (maxBulan - minBulan + 1) ) + minBulan;
var bulan = bulanRandom; // assign nilai variabel bulan disini! (dengan angka antara 1 - 12)
var minTahun = 1900
var maxTahun = 2200
var tahunRandom = Math.floor(Math.random() * (maxTahun - minTahun + 1) ) + minTahun;
var tahun = tahunRandom; // assign nilai variabel tahun disini! (dengan angka antara 1900 - 2200)

console.log("----- Output soal switch case -----")
switch (bulan) {
    case 1: console.log(`${tanggal} Januari ${tahun}`)
        break;
    case 2: console.log(`${tanggal} Februari ${tahun}`)
        break;
    case 3: console.log(`${tanggal} Maret ${tahun}`)
        break;
    case 4: console.log(`${tanggal} April ${tahun}`)
        break;
    case 5: console.log(`${tanggal} Mei ${tahun}`)
        break;
    case 6: console.log(`${tanggal} Juni ${tahun}`)
        break;
    case 7: console.log(`${tanggal} Juli ${tahun}`)
        break;
    case 8: console.log(`${tanggal} Agustus ${tahun}`)
        break;
    case 9: console.log(`${tanggal} September ${tahun}`)
        break;
    case 10: console.log(`${tanggal} Oktober ${tahun}`)
        break;
    case 11: console.log(`${tanggal} November ${tahun}`)
        break;
    case 12: console.log(`${tanggal} Desember ${tahun}`)
        break;

    default: console.log(`Tanggal tidak ditemukan`)
        break;
}