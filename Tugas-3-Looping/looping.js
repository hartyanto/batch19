// =========== Looping while ===========
console.log('------------------------------')
console.log('------  Looping While  -------')
console.log('------------------------------')

var i = 2;
console.log('LOOPING PERTAMA');
while( i <= 20) {
    console.log(`${i} - I love coding`);
    i+=2;
}
console.log('LOOPING KEDUA');
var j = 20;
while( j >= 2){
    console.log(`${j} - I will become a mobile developer`);
    j-=2;
}

// =========== Looping for ===========
console.log('------------------------------')
console.log('------   Looping for   -------')
console.log('------------------------------')

for(var k = 1; k<=20; k++) {
    if( k%3 < 1 && k%2 > 0 ){
        console.log(`${k} - I Love Coding`);
    } else if( k%2 > 0  ) {
        console.log(`${k} - Santai`);
    } else {
        console.log(`${k} - Berkualitas`);
    }
}


// =========== Membuat Persegi Panjang ===========
console.log('-------------------------------')
console.log('--  Membuat Persegi Panjang  --')
console.log('-------------------------------')

var rectangle = [];
for( var baris=0; baris < 4; baris++ ){
    for( var kolom=1; kolom <= 9; kolom++ ){
        if( kolom == 1) {
            rectangle[baris] = "#";
        } else  if( kolom == 9) {
            console.log(rectangle[baris]);
        } else {
            rectangle[baris] += "#";
        }
    }
}

// =========== Membuat Tangga ===========
console.log('------------------------------')
console.log('------  Membuat Tangga  ------')
console.log('------------------------------')

var leader = [];
for( baris=0; baris < 7; baris++ ){
    for( kolom=0; kolom < 7; kolom++){
        if( kolom == 0 ){
            leader[baris] = "#";
        } else if( kolom < baris+1 ){
            leader[baris] += "#";
        }
    }
}

for( i=0; i < 7; i++){
    console.log(leader[i])
}

// =========== Membuat Tangga ===========
console.log('-------------------------------')
console.log('----  Membuat Papan Catur  ----')
console.log('-------------------------------')

i=0;
var chessBoard = [];
while( i < 8 ){
    chessBoard[i] = "";
    i++;
}

for( baris=1; baris <= 8; baris++ ){
    for( kolom=1; kolom <= 8; kolom++){
        if( baris%2 > 0 && kolom%2 < 1 ){
            chessBoard[baris-1] += "#";
        } else if( baris%2 > 0 && kolom%2 > 0 ){
            chessBoard[baris-1] += " ";
        } else if( baris%2 < 1 && kolom%2 < 1 ){
            chessBoard[baris-1] += " ";
        } else {
            chessBoard[baris-1] += "#";
        }
    }
}

for( i=0; i < 8; i++){
    console.log(chessBoard[i]);
}