// =========== Soal No 1 (Range) ===========
console.log('-----------------------------');
console.log('----  Soal No 1 (Range)  ----');
console.log('-----------------------------');

function range(min, max){
    let output = [];
    if( min < max ){
        while( min <= max ){
            output.push(min);
            min++;
        }
        return output;
    } else if( min > max ){
        while( min >= max ){
            output.push(min);
            min--;
        }
        return output;
    } else {
        return -1;
    }
}


console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 
console.log('');


// =========== Soal No 2 (Range with Step) ===========
console.log('-------------------------------------');
console.log('---  Soal No 2 (Range with Step)  ---');
console.log('-------------------------------------');

function rangeWithStep(min, max, step) {
    let output = [];
    if( min < max ){
        while( min <= max ){
            output.push(min);
            min+=step;
        }
        return output;
    } else if( min > max ){
        while( min >= max ){
            output.push(min);
            min-=step;
        }
        return output;
    } else {
        return -1;
    }
}

console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 
console.log('');


// =========== Soal No 3 (Sum of Range) ===========
console.log('----------------------------------');
console.log('---  Soal No 3 (Sum of Range)  ---');
console.log('----------------------------------');

function sum(min, max=0, step=1) {
    let output = 0;
    if( min < max ){
        while( min <= max ){
            output = output + min;
            min+=step;
        }
        return output;
    } else if( min > max ){
        while( min >= max ){
            output = output + min;
            min-=step;
        }
        return output;
    } else {
        return -1;
    }
}

console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 
console.log('');

// =========== Soal No 4 (Array Multidimensi) ===========
console.log('----------------------------------------');
console.log('---  Soal No 4 (Array Multidimensi)  ---');
console.log('----------------------------------------');

function dataHandling(input){
    let output = '';
    for(let i=0; i < input.length; i++){
        output += `Nomor ID: ${input[i][0]} Nama Lengkap: ${input[i][1]} TTL: ${input[i][2]}, ${input[i][3]} Hobi: ${input[i][4]} | `;
    }
    return output;
}

function dataHandling2(input){
    for(let i=0; i < input.length; i++){
        console.log(`Nomor ID: ${input[i][0]}`)
        console.log(`Nama Lengkap: ${input[i][1]}`)
        console.log(`TTL: ${input[i][2]}, ${input[i][3]}`)
        console.log(`Hobi: ${input[i][4]}`)
        console.log('');
    }
    return "done!";
}

var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]

console.log('Output dimasukkan dalam satu variable')
console.log('-------------------------------------')
console.log(dataHandling(input));
console.log('');
console.log('Tampilan output sama seperti soal')
console.log('---------------------------------')
console.log(dataHandling2(input));
console.log('');


// =========== Soal No 5 (Balik Kata) ===========
console.log('--------------------------------');
console.log('---  Soal No 5 (Balik Kata)  ---');
console.log('--------------------------------');

function balikKata(sentence){
    let output = '';

    for( let i=sentence.length-1; i >= 0; i--){
        output += sentence[i];
    }

    return output;
}

console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 
console.log('');


// =========== Soal No 6 (Metode Array) ===========
console.log('----------------------------------');
console.log('---  Soal No 6 (Metode Array)  ---');
console.log('----------------------------------');

function dataHandling3(input){
    input.splice(-1, 1, "Pria", "SMA Internasional Metro");
    input.splice(1, 2, "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung");
    console.log(input)

    let tanggal = input[3].split("/");
    let bulan = parseInt(tanggal[1]);
    switch (bulan) {
        case 01: console.log("Januari");
            break;
        case 02: console.log("Februari");
            break;
        case 03: console.log("Maret");
            break;
        case 04: console.log("April");
            break;
        case 05: console.log("Mei");
            break;
        case 06: console.log("Juni");
            break;
        case 07: console.log("Juli");
            break;
        case 08: console.log("Agustus");
            break;
        case 09: console.log("September");
            break;
        case 10: console.log("Oktober");
            break;
        case 11: console.log("November");
            break;
        case 12: console.log("Desember");
            break;
        default: console.log("Bulan tidak ditemukan")
            break;
    }

    console.log(tanggal.sort(function(a, b){return b-a}));
    console.log(tanggal.join("-"));
    console.log(input[1].slice(0,15));
}

var input2 = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling3(input2);