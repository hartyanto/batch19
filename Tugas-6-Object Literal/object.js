// =========== Soal No 1 (Array to Object) ===========
console.log('---------------------------------------');
console.log('----  Soal No 1 (Array to Object)  ----');
console.log('---------------------------------------');

function arrayToObject(arr) {
    for( let i=0; i < arr.length ; i++ ){
        var temp = arr[i]
        var now = new Date()
        var thisYear = now.getFullYear()
        if( temp[3]  === undefined || temp[3] > thisYear ){
            var age = 'Invalid Birth Year'
        } else {
            var age = thisYear - temp[3]
        }
        var data = {
        "firstName" : temp[0],
        "lastName" : temp[1],
        "gender" : temp[2],
        "age" : age
        }
        var fullName = i+1 + '. ' + data.firstName + ' ' + data.lastName + ':'
        console.log(fullName, data)
    }
}
 
// Driver Code
console.log('Output people');
console.log('-------------');
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 
console.log('');
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/

console.log('Output people2');
console.log('--------------');
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2)
console.log('');
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
// Error case 
console.log('Output Error case');
console.log('-----------------');
arrayToObject([]) // ""
console.log('');


// =========== Soal No 2 (Shopping Time) ===========
console.log('---------------------------------------');
console.log('-----  Soal No 2 (Shopping Time)  -----');
console.log('---------------------------------------');

function shoppingTime(memberId, money) {
    let listPurchased = []
    let produk = {
        'Sepatu Stacattu': 1500000,
        'Baju Zoro': 500000,
        'Baju H&N': 250000,
        'Sweater Uniklooh': 175000,
        'Casing Handphone': 50000
    }

    if( memberId == '' || memberId === undefined ){
        return 'Mohon maaf, toko X hanya berlaku untuk member saja'
    } else if( money < 50000 ){
        return 'Mohon maaf, uang tidak cukup'
    }

    var changeMoney = money;
    while( changeMoney >= produk["Casing Handphone"] ){
        if( changeMoney >= produk["Sepatu Stacattu"] ){
            listPurchased.push('Sepatu Stacattu')            
            changeMoney = changeMoney-produk["Sepatu Stacattu"]
        } else if ( changeMoney >= produk["Baju Zoro"] ){
            listPurchased.push('Baju Zoro')      
            changeMoney = changeMoney-produk["Baju Zoro"]
        } else if ( changeMoney >= produk["Baju H&N"] ){
            listPurchased.push('Baju H&N')     
            changeMoney = changeMoney-produk["Baju H&N"]
        } else if ( changeMoney >= produk["Sweater Uniklooh"] ){
            listPurchased.push('Sweater Uniklooh')
            changeMoney = changeMoney-produk["Sweater Uniklooh"]
        } else {
            listPurchased.push('Casing Handphone')
            changeMoney = changeMoney-produk["Casing Handphone"]
            break;
        }
    }
   
    var output = {
        'memberId': memberId,
        'money': money,
        'listPurchased': listPurchased,
        'changeMoney': changeMoney
    }

    return output
}

// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));
//{ memberId: '1820RzKrnWn08',
// money: 2475000,
// listPurchased:
//  [ 'Sepatu Stacattu',
//    'Baju Zoro',
//    'Baju H&N',
//    'Sweater Uniklooh',
//    'Casing Handphone' ],
// changeMoney: 0 }
console.log(shoppingTime('82Ku8Ma742', 170000));
//{ memberId: '82Ku8Ma742',
// money: 170000,
// listPurchased:
//  [ 'Casing Handphone' ],
// changeMoney: 120000 }
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja
console.log('');

// =========== Soal No 3 (Naik Angkot) ===========
console.log('---------------------------------------');
console.log('------  Soal No 3 (Naik Angkot)  ------');
console.log('---------------------------------------');


function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    let data = []
    
    for(let i=0 ; i < arrPenumpang.length ; i++){
        let bayar = 0
        let penumpang = arrPenumpang[i][0]
        let naikDari = arrPenumpang[i][1]
        let tujuan = arrPenumpang[i][2]
        for( let j = 0 ; j < rute.length ; j++ ){
            if( rute[j] == naikDari ){
                for( j ; j < rute.length ; j++){
                    if( rute[j] == tujuan ) break;
                    bayar += 2000
                }
                break;
            }
        }
        data[i] = {
            'penumpang': penumpang,
            'naikDari': naikDari,
            'tujuan': tujuan,
            'bayar': bayar
        }
    }

    return data
}

//TEST CASE
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]

console.log(naikAngkot([])); //[]