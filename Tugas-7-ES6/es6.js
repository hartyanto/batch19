// =========== Mengubah fungsi menjadi fungsi arrow ===========
console.log('----------------------------------------------------');
console.log('------  Mengubah fungsi menjadi fungsi arrow  ------');
console.log('----------------------------------------------------');

/* 
const golden = function goldenFunction(){
    console.log("this is golden!!");
}
*/

// ----- ES 6 -----
/*
const golden = goldenFunction = () => {
    console.log('this is golden!!');
}
*/

// ----- ES6 one line -----
const golden = () => console.log('this is golden!!');
golden();
console.log('');


// =========== Sederhanakan menjadi Object literal di ES6 ===========
console.log('----------------------------------------------------');
console.log('---  Sederhanakan menjadi Object literal di ES6  ---');
console.log('----------------------------------------------------');

/*
const newFunction = function literal(firstName, lastName){
    return {
    firstName: firstName,
    lastName: lastName,
    fullName: function(){
        console.log(firstName + " " + lastName)
        return 
        }
    }
}
*/
const newFunction = (firstName, lastName) => {
    return {
        firstName,
        lastName,
        fullName: () => console.log(`${firstName} ${lastName}`)
        }
}

//Driver Code 
newFunction("William", "Imoh").fullName();
console.log(newFunction("William", "Imoh").firstName);
console.log(newFunction("William", "Imoh").lastName);
console.log('');


// =========== Destructuring ===========
console.log('---------------------------------------------------');
console.log('-----------------  Destructuring  -----------------');
console.log('---------------------------------------------------');

const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
}

const { firstName, lastName, destination, occupation, spell } = newObject;

// Driver code
console.log(firstName, lastName, destination, occupation, spell);
console.log('');


// =========== Array Spreading ===========
console.log('---------------------------------------------------');
console.log('----------------  Array Spreading  ----------------');
console.log('---------------------------------------------------');

const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]

const combined = [...west, ...east]
//Driver Code
console.log(combined);
console.log('');


// =========== Template Literals ===========
console.log('---------------------------------------------------');
console.log('---------------  Template Literals  ---------------');
console.log('---------------------------------------------------');

const planet = "earth"
const view = "glass"
var before = 'Lorem ' + view + 'dolor sit amet, ' +  
    'consectetur adipiscing elit,' + planet + 'do eiusmod tempor ' +
    'incididunt ut labore et dolore magna aliqua. Ut enim' +
    ' ad minim veniam'
var after = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet} do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`

// Driver Code
console.log(before) 
console.log('') 
console.log(after) 