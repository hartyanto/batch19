// =========== Soal No. 1 (Callback Baca Buku) ===========
console.log('---------------------------------------------------');
console.log('--------  Soal No. 1 (Callback Baca Buku)  --------');
console.log('---------------------------------------------------');

var readBooks = require('./callback.js')

var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]

let data1 = books[0]
let data2 = books[1]
let data3 = books[2]
let waktu = 10000

readBooks(waktu, data1, (sisaWaktu) => {
    readBooks(sisaWaktu, data2, (sisaWaktu) => {
        readBooks(sisaWaktu, data3, (time) => {
        })
    })
})

let waitTime = 0
for ( let a = 0 ; a < books.length ; a++) waitTime+=books[a].timeSpent
let waitTimeLoop = 0
setTimeout(()=>{
    console.log('')
    console.log('Menggunakan looping')
    console.log('-------------------')
    for( let i = 0; i < books.length ; i++){
        setTimeout( ()=>{
            readBooks( waktu, books[i], (sisaWaktu) => waktu = sisaWaktu )
        }, waitTimeLoop)
        waitTimeLoop+=books[i].timeSpent+100    
    }
}, waitTime+100)