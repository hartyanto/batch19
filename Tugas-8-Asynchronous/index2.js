// =========== Soal No. 2 (Promise Baca Buku) ===========
console.log('----------------------------------------------------');
console.log('---------  Soal No. 2 (Promise Baca Buku)  ---------');
console.log('----------------------------------------------------');

var readBooksPromise = require('./promise.js')

var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]

// Lanjutkan code untuk menjalankan function readBooksPromise
readBooksPromise(10000, books[0])
    .then((sisawaktu) => readBooksPromise(sisawaktu, books[1]))
    .then((sisawaktu) => readBooksPromise(sisawaktu, books[2]))
    .catch((sisawaktu) => console.log(sisawaktu))