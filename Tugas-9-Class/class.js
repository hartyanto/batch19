// =========== 1. Animal Class - Releaese 0 ===========
console.log('--------------------------------------------------');
console.log('---------  1. Animal Class - Releaese 0  ---------');
console.log('--------------------------------------------------');

class Animal {
    constructor(name, legs = 4, cold_blooded = false) {
        this.name = name
        this.legs = legs
        this.cold_blooded = cold_blooded
    }
}

var sheep = new Animal("shaun");

console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false
console.log('')

console.log('----------------------------------------------');
console.log('--------- Animal Class - Releaese 1  ---------');
console.log('----------------------------------------------');

class Ape extends Animal {
    constructor(name, legs = 2, cold_blooded) {
        super(name, legs, cold_blooded)
    }
    yell() {
        return console.log('Auooo')
    }
}

class Frog extends Animal {
    constructor(name, legs, cold_blooded) {
        super(name, legs, cold_blooded)
    }
    jump() {
        return console.log('hop hop');
    }
}

var sungokong = new Ape("kera sakti")
console.log(sungokong.name)
console.log(sungokong.legs)
console.log(sungokong.cold_blooded)
sungokong.yell() // "Auooo"
console.log('');

var kodok = new Frog("buduk")
console.log(kodok.name)
console.log(kodok.legs)
console.log(kodok.cold_blooded)
kodok.jump() // "hop hop" 


// =========== 2. Function to Class ===========
console.log('--------------------------------------------------');
console.log('-------------  2. Function to Class  -------------');
console.log('--------------------------------------------------');

function Clock({ template }) {
    var timer;

    function render() {
        var date = new Date();

        var hours = date.getHours();
        if (hours < 10) hours = '0' + hours;

        var mins = date.getMinutes();
        if (mins < 10) mins = '0' + mins;

        var secs = date.getSeconds();
        if (secs < 10) secs = '0' + secs;

        var output = template
        .replace('h', hours)
        .replace('m', mins)
        .replace('s', secs);

        console.log(output);
    }

    this.stop = function() {
        clearInterval(timer);
    };

    this.start = function() {
        render();
        timer = setInterval(render, 1000);
    };
}

var clock = new Clock({template: 'h:m:s'});
console.log('Clock Function');
clock.start(); 
setTimeout(()=>{
    clock.stop()
    console.log('');
}, 10000)

// =========== Class Clock ===========
class ClockClass {
    constructor({template}){
        this.timer = ''
        this.output = template
    }

    render() {
        this.date = new Date
        this.secs = this.date.getSeconds()
        this.mins = this.date.getMinutes()
        this.hours = this.date.getHours()
        if ( this.secs < 10 ) this.secs = '0' + this.secs
        if ( this.mins < 10 ) this.mins = '0' + this.mins
        if ( this.hours < 10 ) this.hours = '0' + this.hours

        this.output = 'h:m:s'.replace('h', this.hours).replace('m', this.mins).replace('s', this.secs)
        console.log(this.output);
    }

    start() {
        this.render()
        this.timer = setInterval(this.render, 1000)
    }
    stop() {
        clearInterval(this.timer)
    }
}

var clock2 = new ClockClass({template: 'h:m:s'});
setTimeout(() => {
    console.log('Clock Class');
    clock2.start();
    setTimeout(()=>{
        clock2.stop()
    }, 10000)
}, 10100)